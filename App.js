import React from 'react';
import { createAppContainer } from 'react-navigation';
import { Root } from 'native-base';
import AppNavigator from './app/screen/index';

const AppContainer = createAppContainer(AppNavigator);

const App = () => <Root><AppContainer /></Root>;

export default App;
