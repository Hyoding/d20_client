module.exports = {
  root: true,
  parser: 'babel-eslint',
  globals: {
    fetch: true,
  },
  extends: 'airbnb',
  "rules": {
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
  }
};
