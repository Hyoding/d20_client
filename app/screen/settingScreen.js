import React from 'react';
import {
  Container, Content, Card, CardItem, Text, Header, Title,
  Button, Left, Right, Body, Icon,
} from 'native-base';
import Slider from '@react-native-community/slider';
import Util from '../util/util';

const MinRadius = 100;
const MaxRadius = 3000;

export default class SettingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      radius: null,
    };
  }

  onSlidingComplete = (radius) => {
    Util.saveRadius(radius);
    this.setState({ radius });
  }

  onSlidingChange = radius => this.setState({ radius });

  componentDidMount = async () => {
    const radius = await Util.getRadius();
    this.setState({ radius });
  }

  render() {
    const { radius } = this.state;
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={this.props.navigation.openDrawer}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Setting</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem header>
              <Text>Search Filter</Text>
            </CardItem>
            <CardItem>
              <Text>Radius in meters</Text>
            </CardItem>
            <Slider
              style={{ width: '90%', alignSelf: 'center' }}
              minimumValue={MinRadius}
              maximumValue={MaxRadius}
              step={100}
              value={radius}
              onValueChange={this.onSlidingChange}
              onSlidingComplete={this.onSlidingComplete}
            />
            <CardItem style={{ justifyContent: 'space-between' }}>
              <Text>{MinRadius}</Text>
              <Text>{radius}</Text>
              <Text>{MaxRadius}</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
