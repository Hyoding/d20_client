import React from 'react';
import {
  Container, Content, Card, CardItem, Text, Header, Title,
  Button, Left, Right, Body, Icon, Form, Textarea,
} from 'native-base';
import Util from '../util/util';
import AppConfig from '../config';

export default class FeedbackScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      feedback: '',
    };
  }

  sendFeedback = async () => {
    const feedbackMinLength = 10;
    const { feedback } = this.state;
    if (feedback.length < feedbackMinLength) {
      Util.showToast(`Feedback should be at least ${feedbackMinLength} characters long`);
      return;
    }
    console.log('Sending feedback', feedback);
    const rawResponse = await this.postData(`${AppConfig.API_ENDPOINT}/email/sendFeedback`, { userKey: Util.getDeviceUniqueId(), feedback });
    if (rawResponse.status === 200) {
      const response = await rawResponse.json();
      const { message } = response;
      Util.showToast(message);
      this.setState({ feedback: '' });
    } else {
      alert('Something went wrong, if the problem persists, please contact the developer');
    }
  }

  postData = async (url = '', data = {}) => {
    const rawResponse = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    return rawResponse;
  }

  render() {
    const { feedback } = this.state;
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={this.props.navigation.openDrawer}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Feedback</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem header>
              <Text>Please share how we can improve</Text>
            </CardItem>
            <Form>
              <Textarea
                style={{ width: '95%', alignSelf: 'center' }}
                rowSpan={5}
                bordered
                onChangeText={(value) => { this.setState({ feedback: value }); }}
                value={feedback}
              />
            </Form>
            <CardItem>
              <Button small info onPress={this.sendFeedback}>
                <Text>SEND</Text>
              </Button>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
