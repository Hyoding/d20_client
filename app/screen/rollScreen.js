import React from 'react';
import {
  Container, Header, Footer, FooterTab, Title, Left, Icon, Right, Button,
  Body, Content, Text, Card, CardItem,
  H1, H3, Spinner,
} from 'native-base';
import { Image, Linking } from 'react-native';
import RollRequest from '../model/rollRequest';
import AppConfig from '../config';
import LocationUtil from '../util/locationUtil';
import Util from '../util/util';

export default class RollScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isRolling: true,
      restaurant: null,
    };
  }

  generateURL = (rollRequest) => {
    const {
      userKey, latitude, longitude, radiusInMeters,
    } = rollRequest;
    return `${AppConfig.API_ENDPOINT}/roll?userKey=${userKey}&latitude=${latitude}&longitude=${longitude}&radiusInMeters=${radiusInMeters}`;
  }

  roll = async () => {
    await this.setState({ isRolling: true });

    const location = await LocationUtil.getCurrentLocation();
    const radius = await Util.getRadius();
    const rollRequest = new RollRequest({
      userKey: Util.getDeviceUniqueId(),
      latitude: location.latitude,
      longitude: location.longitude,
      radius,
    });
    const restaurantApiCall = await fetch(this.generateURL(rollRequest));
    const statusCode = restaurantApiCall.status;
    if (statusCode === 200) {
      const rollResponse = await restaurantApiCall.json();
      const { restaurant, numberOpened } = rollResponse;
      this.setState({
        restaurant, numberOpened, isRolling: false,
      });
    } else if (statusCode === 204) {
      alert('No restaurants are available. Either change search radius or try at another time');
      this.setState({ restaurant: null, isRolling: false });
    } else {
      // TODO: Display error message
      alert('Something went wrong, if the problem persists, please contact the developer');
    }
  }

  openLink = (url) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert("Don't know how to open URI: " + url);
      }
    });
  };

  componentDidMount = async () => {
    if (await LocationUtil.hasLocationPermission()) {
      this.roll();
    } else if (await LocationUtil.requestLocationPermission()) {
      this.roll();
    }
  }

  render() {
    const {
      isRolling, restaurant, numberOpened,
    } = this.state;
    let content;
    if (isRolling) {
      content = (
        <Spinner color="blue" />
      );
    } else if (!isRolling && !restaurant) {
      content = (
        <Card transparent />
      );
    } else {
      const {
        address, imageUrl, name, rating, price, url, distance,
      } = restaurant;
      content = (
        <Card>
          <CardItem>
            <Body>
              <H3 style={{ marginBottom: '3%' }}>{`Picked from ${numberOpened} options`}</H3>
              <H1>{name}</H1>
            </Body>
          </CardItem>
          <CardItem cardBody>
            <Image
              source={{ uri: imageUrl }}
              style={{ flex: 1, height: 300, width: null }}
            />
          </CardItem>
          <CardItem>
            <Body>
              <H3 style={{ marginBottom: '2%' }}>{`${address.address}, ${address.city}`}</H3>
              <Text>{`Distance: ${Math.trunc(distance)} m`}</Text>
              <Text>{`Rating: ${rating}`}</Text>
              <Text>{`Price: ${price || 'n/a'}`}</Text>
              <Button transparent onPress={() => { this.openLink(url); }}>
                <Text>DETAILS</Text>
              </Button>
            </Body>
          </CardItem>
        </Card>
      );
    }

    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={this.props.navigation.openDrawer}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>D20</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          { content }
        </Content>
        <Footer>
          <FooterTab>
            <Button info full onPress={this.roll}>
              <Text style={{ color: 'white' }}>ROLL AGAIN</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
