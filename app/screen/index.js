import { createDrawerNavigator } from 'react-navigation';
import RollScreen from './rollScreen';
import SettingScreen from './settingScreen';
import FeedbackScreen from './feedbackScreen';

const RollScreenRouter = createDrawerNavigator(
  {
    Roll: { screen: RollScreen },
    Setting: { screen: SettingScreen },
    Feedback: { screen: FeedbackScreen },
  },
  {
    hideStatusBar: true,
  },
);

export default RollScreenRouter;
