function RollRequest(obj) {
  this.userKey = obj.userKey;
  this.latitude = obj.latitude;
  this.longitude = obj.longitude;
  this.radiusInMeters = obj.radius;
}
export default RollRequest;
