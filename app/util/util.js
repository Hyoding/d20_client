import AsyncStorage from '@react-native-community/async-storage';
import {
  Toast,
} from 'native-base';

import DeviceInfo from 'react-native-device-info';

async function storeData(key, value) {
  try {
    await AsyncStorage.setItem(key, value);
    return true;
  } catch (e) {
    return false;
  }
}

async function getData(key, defaultValue) {
  try {
    const value = await AsyncStorage.getItem(key);
    return value || defaultValue || null;
  } catch (e) {
    return defaultValue || null;
  }
}

const RadiusKey = 'radius';
const DefaultRadius = 500;

const Util = {
  getData: async (key, defaultValue) => getData(key, defaultValue),
  saveData: async (key, value) => storeData(key, value),
  getRadius: async () => {
    const result = await Util.getData(RadiusKey, DefaultRadius);
    return Number(result);
  },
  saveRadius: async radius => Util.saveData(RadiusKey, radius.toString()),
  showToast: (msg, duration = 5000) => Toast.show({
    text: msg,
    duration,
    position: 'top',
  }),
  getDeviceUniqueId: () => DeviceInfo.getUniqueID(),
};

export default Util;
